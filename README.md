# List Users


## Task
https://reqres.in/ is a portal with public APIs for testing purposes. In your task, you will be using its „LIST USERS” GET API: https://reqres.in/api/users

Please implement a simple app using tools of your choice that will provide the following functionalities:
1. A user can load data from a remote source.
2. A user can view loaded data.
3. A user can mark (and unmark) some of the users from the list as their favourites.

### Data list screen:
1. Display data in a list
2. Data’s favourite state should be marked with the proper icon
3. Display button to filter only favourite data
4. Data should be fetched automatically but with an option to manually refresh

### Data screen:
1. From the data list screen, a user can navigate to a data screen 
2. On the data screen display details of a selected user

### Bonus:
1. save the app state (data with favourite state) to some persistence storage (offline or online) to
not erase the data after the next launch
2. support light and dark themes

### Notes:

1. Consider that the API uses pagination
2. Consider adding some tests
