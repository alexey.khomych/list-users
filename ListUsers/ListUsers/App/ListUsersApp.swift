//
//  ListUsersApp.swift
//  ListUsers
//
//  Created by Alexey Khomych on 03.03.2023.
//

import SwiftUI

@main
struct ListUsersApp: App {
    
    // MARK: - Public Variables
    
    let persistenceController = PersistenceController.shared
    
    // MARK: - Private Variables
    
    @StateObject private var store = UserStore()

    var body: some Scene {
        WindowGroup {
            NavigationView {
                HomeView(userList: $store.userList)
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
                    .environment(\.userStore, store)
            }
            .task {
                await store.load()
            }
        }
    }
}
