//
//  HomeView.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import SwiftUI

struct HomeView: View {
    
    // MARK: - Private Variables
    
    @Environment(\.userStore) var userStore
    @Environment(\.managedObjectContext) private var context
    
    @State private var isLoading = false
    
    @FetchRequest(
        entity: Like.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Like.userId, ascending: false)
        ],
        animation: .easeInOut(duration: 0.3)
    ) private var likeList: FetchedResults<Like>
    
    // MARK: - Public Variables
    
    @Binding var userList: [UserModel]
    
    var body: some View {
        VStack(alignment: .leading) {
            List {
                ForEach($userList.indices, id: \.self) { index in
                    let userModel = userList[index]
                    
                    NavigationLink(
                        destination: UserView(userModel: $userList[index])
                            .environment(\.managedObjectContext, context)
                    ) {
                        CardView(userModel: $userList[index])
                    }
                    .onAppear {
                        listItemAppears(userModel)
                    }
                }
                .onAppear() {
                    compareData()
                }
            }
            .navigationTitle(LS.Home.titleText)
            .refreshable {
                await userStore.refreshData()
                compareData()
            }
        }
    }
}

// MARK: - Private

private extension HomeView {
    
    func listItemAppears<Item: Identifiable>(_ item: Item) {
        if !userList.isLastItem(item) {
            return
        }
        
        isLoading = true
        
        Task {
            await userStore.load()
            isLoading = false
        }
    }
    
    func compareData() {
        userList.forEach { user in
            user.isLiked = likeList.first(where: { $0.userId == user.id })?.isLiked
        }
    }
}
