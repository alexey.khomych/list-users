//
//  UserModel.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

final class UserModel: Identifiable {

    // MARK: - Public Variables
    
    let id: Int
    let email: String
    let firstName: String
    let lastName: String
    let avatar: String
    var isLiked: Bool?
    
    // MARK: - Computed Variable
    
    var fullName: String {
        return firstName + " " + lastName
    }
    
    // MARK: - Initialization
    
    init(id: Int, email: String, firstName: String, lastName: String, avatar: String) {
        self.id = id
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.avatar = avatar
    }
}

// MARK: - Codable

extension UserModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case avatar
    }
}

// MARK: - SampleData

extension UserModel {
    
    static let sampleData: [UserModel] = [
        UserModel(
            id: 0,
            email: "test@gmail.com",
            firstName: "Daiv",
            lastName: "Krok",
            avatar: ""
        )
    ]
}
