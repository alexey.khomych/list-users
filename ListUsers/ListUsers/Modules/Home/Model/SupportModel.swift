//
//  SupportModel.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

struct SupportModel: Codable {
    let url: String
    let text: String
}
