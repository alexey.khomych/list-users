//
//  UserView.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import SwiftUI

struct UserView: View {
    
    // MARK: - Public Variables
    
    @Binding var userModel: UserModel
    
    @Environment(\.managedObjectContext) private var context
    
    var body: some View {
        VStack {
            HStack {
                AsyncImage(
                    url: URL(string: userModel.avatar),
                    content: { image in
                        image.resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(maxWidth: 70, maxHeight: 70)
                            .clipShape(Circle())
                    },
                    placeholder: {
                        ProgressView()
                    }
                )
                VStack(alignment: .leading) {
                    Label(userModel.fullName, systemImage: "person.fill")
                        .font(.headline)
                    Label(userModel.email, systemImage: "mail.fill")
                        .font(.caption)
                }
                Spacer()
            }
            Button {
                changeReaction()
            } label: {
                Label(userModel.isLiked ?? false ? "Remove from Favorite" : "Add to Favorite", systemImage: userModel.isLiked ?? false ? "heart.fill" : "heart")
                    .foregroundColor(.indigo)
                    .font(.headline)
            }
            .font(.caption)
            .padding(.top)

            Spacer()
        }
        .padding(.leading)
    }
}

// MARK: - Private

private extension UserView {
    
    func changeReaction() {
        
        
        if userModel.isLiked == nil {
            userModel.isLiked = true
        } else {
            userModel.isLiked?.toggle()
        }
        
        do {
            let like = Like(context: context)
            like.userId = Int64(userModel.id)
            like.isLiked = userModel.isLiked!
            
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
}

// MARK: - PreviewProvider

struct UserView_Previews: PreviewProvider {
    
    static var userModel = UserModel.sampleData[0]
    
    static var previews: some View {
        UserView(userModel: .constant(UserModel.sampleData[0]))
    }
}
