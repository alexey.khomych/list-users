//
//  CardView.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import SwiftUI

struct CardView: View {
    
    // MARK: - Public Variables
    
    @Binding var userModel: UserModel
    
    // MARK: - Public
    
    var body: some View {
        HStack {
            AsyncImage(
                url: URL(string: userModel.avatar),
                content: { image in
                    image.resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 50, maxHeight: 50)
                        .clipShape(Circle())
                },
                placeholder: {
                    ProgressView()
                }
            )
            Spacer()
            VStack(alignment: .leading) {
                Text(userModel.fullName)
                    .font(.headline)
                Text(userModel.email)
                    .font(.caption)
            }
            Spacer()
            Image(systemName: userModel.isLiked ?? false ? "heart.fill" : "heart")
        }
        .padding()
    }
}

struct CardView_Previews: PreviewProvider {
    
    static var userModel = UserModel.sampleData[0]
    
    static var previews: some View {
        CardView(userModel: .constant(UserModel.sampleData[0]))
            .previewLayout(.fixed(width: 400, height: 60))
    }
}
