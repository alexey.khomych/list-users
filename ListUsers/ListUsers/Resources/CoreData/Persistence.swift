//
//  Persistence.swift
//  ListUsers
//
//  Created by Alexey Khomych on 03.03.2023.
//

import CoreData

struct PersistenceController {
    
    // MARK: - Public Variables
    
    static let shared = PersistenceController()

    let container: NSPersistentContainer

    // MARK: - Initialization
    
    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "ListUsers")
        container.viewContext.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        container.viewContext.automaticallyMergesChangesFromParent = true
    }
}
