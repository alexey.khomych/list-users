//
//  UserTransactionsKey.swift
//  ListUsers
//
//  Created by Alexey Khomych on 06.03.2023.
//

import SwiftUI

struct UserTransactionsKey: EnvironmentKey {
    
    static let defaultValue = UserStore()
}
