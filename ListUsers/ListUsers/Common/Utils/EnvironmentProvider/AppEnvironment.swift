//
//  Environment.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

protocol EnvironmentType {
    var baseUrl: String { get }
}

/// Project environment public variables
struct AppEnvironment: EnvironmentType {
    var baseUrl: String
}

