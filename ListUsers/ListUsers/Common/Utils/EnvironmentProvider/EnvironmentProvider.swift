//
//  EnvironmentProvider.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

protocol EnvironmentProvider {
    associatedtype T
    func loadEnviroment() -> T
}

private struct EnvironmentKeys {
    static let baseUrl = "Base_url"
}

/// Loads app settings for each environment. It will crash if don't find key.
class EnvironmentProviderImpl: EnvironmentProvider {
    
    func loadEnviroment() -> EnvironmentType {
        let bundle = Bundle(for: type(of: self))

        guard let infoDict = bundle.infoDictionary else {
            fatalError("No info plist configuration in bundle!")
        }
        
        guard let baseUrl = infoDict[EnvironmentKeys.baseUrl] as? String else {
            fatalError("Info.plist has no baseUrl for needed configuration!")
        }
        
        return AppEnvironment(
            baseUrl: baseUrl
        )
    }
}
