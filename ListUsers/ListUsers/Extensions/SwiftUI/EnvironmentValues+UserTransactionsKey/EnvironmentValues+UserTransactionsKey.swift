//
//  EnvironmentValues+UserTransactionsKey.swift
//  ListUsers
//
//  Created by Alexey Khomych on 06.03.2023.
//

import SwiftUI

extension EnvironmentValues {
    
    var userStore: UserStore {
        get {
            self[UserTransactionsKey.self]
        } set {
            self[UserTransactionsKey.self] = newValue
        }
    }
}
