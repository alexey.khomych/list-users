//
//  String+Localized.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

extension String {
    
    func localized() -> String {
        return localized(using: nil, in: .main)
    }
    
    func localizedFormat(_ arguments: CVarArg...) -> String {
        return String(format: localized(), arguments: arguments)
    }
    
    func pluralizedFormat(_ arguments: CVarArg...) -> String {
        return withVaList(arguments) {
            NSString(format: localized(), locale: NSLocale.current, arguments: $0) as String
        }
    }
    
    func localized(in bundle: Bundle?) -> String {
        return localized(using: nil, in: bundle)
    }
    
    func localized(using tableName: String?, in bundle: Bundle?) -> String {
        let bundle: Bundle = bundle ?? .main
        return NSLocalizedString(self, tableName: tableName, bundle: bundle, value: "", comment: "")
    }
}
