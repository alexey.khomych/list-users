//
//  HTTPURLResponse.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

extension HTTPURLResponse {
    
    ///parse error code and return localized error message
    func statusCode() -> NetworkError? {
        switch statusCode {
        case 200...299:
            return nil
        case 400:
            return .serverError
        case 401...500:
            return .uknownError
        case 501...599:
            return .serverError
        case 600:
            return .uknownError
        default:
            return .uknownError
        }
    }
}
