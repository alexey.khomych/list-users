//
//  UserStore.swift
//  ListUsers
//
//  Created by Alexey Khomych on 05.03.2023.
//

import Foundation
import SwiftUI
import CoreData

final class UserStore: ObservableObject {
    
    // MARK: - Public Variables
    
    @Published var userList: [UserModel] = []
    
    // MARK: - Private Variables
    
    private var currentPage = Constants.startPageNumber
    private var totalPages: Int?
    private let networkService = UserAPIImpl()
    
    // MARK: - Public
    
    func load() async {
        if let totalPages, currentPage > totalPages {
            return
        }
        
        do {
            let responseModel = try await networkService.list(page: currentPage)
            totalPages = responseModel.totalPages
            currentPage += 1

            await MainActor.run {
                self.userList.append(contentsOf: responseModel.data)
            }
        } catch  {
            print(error.localizedDescription)
        }
    }
    
    func refreshData() async {
        do {
            let responseModel = try await networkService.list()
            totalPages = responseModel.totalPages
            currentPage = Constants.startPageNumber

            await MainActor.run {
                self.userList = responseModel.data
            }
        } catch  {
            print(error.localizedDescription)
        }
    }
}

// MARK: - Private

private extension UserStore {
    
    enum Constants {
        static let startPageNumber = 1
    }
}
