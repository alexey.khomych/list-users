//
//  UserAPI.swift
//  ListUsers
//
//  Created by Alexey Khomych on 05.03.2023.
//

import Foundation

protocol UserAPI {
    func list(page: Int) async throws -> UserResponseModel
}

final class UserAPIImpl: UserAPI {
    
    // MARK: - Private Variables
    
    private let router = NetworkRouter<UserEndPoint>()
    
    // MARK: - Public
    
    func list(page: Int = 1) async throws -> UserResponseModel {
        try await withCheckedThrowingContinuation { continuation in
            router.request(.list(page: page)) { response in
                let result = response.handle(UserResponseModel.self)
                
                switch result {
                case let .failure(error):
                    continuation.resume(throwing: error)
                case let .success(responseModel):
                    continuation.resume(returning: responseModel)
                }
            }
        }
    }
}
