//
//  UserEndPoint.swift
//  ListUsers
//
//  Created by Alexey Khomych on 05.03.2023.
//

import Foundation

public enum UserEndPoint {
    case list(page: Int)
}

// MARK: - EndPointType

extension UserEndPoint: EndPointType {
    
    var path: String {
        switch self {
        case .list:
            return "users"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .list:
            return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case let .list(page):
            return .requestParameters(
                bodyParameters: nil,
                bodyEncoding: .urlEncoding,
                urlParameters: [
                    "page" : page
                ]
            )
        }
    }
}
