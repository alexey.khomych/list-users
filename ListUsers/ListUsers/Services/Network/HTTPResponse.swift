//
//  HTTPResponse.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

struct HTTPResponse {
    
    // MARK: - Public Variables
    
    let data: Data?
    let response: URLResponse?
    let error: Error?
    
    // MARK: - Public
    
    func handle<T>(_ type: T.Type) -> (Result<T, Error>) where T : Decodable {
        guard let response = response as? HTTPURLResponse else {
            return .failure(NetworkError.badRequest)
        }
        
        if let statusCodeError = response.statusCode() {
            return .failure(statusCodeError)
        }
        
        guard let responseData = data else {
            return .failure(NetworkError.badRequest)
        }

        do {
            let apiResponse = try JSONDecoder().decode(type, from: responseData)
            
            return .success(apiResponse)
        } catch {
            return .failure(NetworkError.badRequest)
        }
    }
}
