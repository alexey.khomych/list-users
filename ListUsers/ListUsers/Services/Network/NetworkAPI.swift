//
//  NetworkAPI.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

/// here base url could be initialized for different environments
class NetworkAPI {

    static let environment: NetworkEnvironment = .stage
}
