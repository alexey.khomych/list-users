//
//  NetworkRouter.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

protocol NetworkRouterProtocol {
    associatedtype EndPoint: EndPointType
    
    func request(_ route: EndPoint, completion: @escaping (_ response: HTTPResponse) -> ())
    func cancel()
}

class NetworkRouter<EndPoint: EndPointType>: NetworkRouterProtocol {
    
    // MARK: - Private Variables
    
    private var task: URLSessionTask?
    
    // MARK: - Public
    
    func request(_ route: EndPoint, completion: @escaping (_ response: HTTPResponse) -> ()) {
        do {
            let request = try self.buildRequest(from: route)
            
            task = URLSession.shared.dataTask(with: request) { data, response, error in
                let response = HTTPResponse(data: data, response: response, error: error)
                completion(response)
            }
            task?.resume()
        } catch {
            let response = HTTPResponse(data: nil, response: nil, error: error)
            completion(response)
        }
    }
    
    func cancel() {
        task?.cancel()
    }
}

// MARK: - buildRequest

extension NetworkRouter {
    
    fileprivate func buildRequest(from route: EndPoint) throws -> URLRequest {
        var request = URLRequest(
            url: route.baseUrl.appendingPathComponent(route.path),
            cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
            timeoutInterval: 10
        )
        
        request.httpMethod = route.httpMethod.rawValue
        
        if let headers = route.headers {
            addAdditionalHeaders(headers, &request)
        }
        
        do {
            switch route.task {
            case .request:
                break
            case .requestParameters(
                let bodyParameters,
                let bodyEncoding,
                let urlParameters
            ):

                try configureParameters(
                    bodyParameters: bodyParameters,
                    bodyEncoding: bodyEncoding,
                    urlParameters: urlParameters,
                    request: &request
                )
            case .requestParametersAndHeaders(
                let bodyParameters,
                let bodyEncoding,
                let urlParameters,
                let additionalHeaders
            ):
                addAdditionalHeaders(additionalHeaders, &request)
                try configureParameters(
                    bodyParameters: bodyParameters,
                    bodyEncoding: bodyEncoding,
                    urlParameters: urlParameters,
                    request: &request
                )
            }
            return request
        } catch {
            throw error
        }
    }
}

// MARK: - configureParameters

extension NetworkRouter {
    
    fileprivate func configureParameters(
        bodyParameters: Parameters?,
        bodyEncoding: ParameterEncoding,
        urlParameters: Parameters?,
        request: inout URLRequest
    ) throws {
        do {
            try bodyEncoding.encode(
                urlRequest: &request,
                bodyParameters: bodyParameters,
                urlParameters: urlParameters
            )
        } catch {
            throw error
        }
    }
}

// MARK: - AdditionalHeaders

extension NetworkRouter {
    
    func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, _ request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
