//
//  ErrorModel.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

struct ErrorModel {
    var error: [String]?
}

// MARK: - Codable

extension ErrorModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case error
    }
}
