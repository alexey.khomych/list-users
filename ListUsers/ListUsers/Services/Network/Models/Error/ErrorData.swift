//
//  ErrorData.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

struct ErrorData: Codable {
    var data: ErrorDataModel?
}
