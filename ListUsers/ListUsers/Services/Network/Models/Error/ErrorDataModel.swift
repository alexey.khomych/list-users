//
//  ErrorDataModel.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

struct ErrorDataModel: Codable {
    var httpStatus: Int?
    var errorCode: String?
    var developerMessage: String?
    var message: String?
    var title: String?
    var text: String?
    var type: String?
}
