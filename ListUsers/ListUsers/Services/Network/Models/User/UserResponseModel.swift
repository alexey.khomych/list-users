//
//  UserResponseModel.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

struct UserResponseModel {
    let page: Int
    let perPage: Int
    let total: Int
    let totalPages: Int
    let data: [UserModel]
    let support: SupportModel
}

// MARK: - Codable

extension UserResponseModel: Codable {
   
    enum CodingKeys: String, CodingKey {
        case page
        case perPage = "per_page"
        case total
        case totalPages = "total_pages"
        case data, support
    }
}
