//
//  NetworkError.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

public enum NetworkError: String, Error {
    case encodingFailed = "Parameters encoding failed."
    case missingUrl = "URL is nil."
    case badRequest = "Invalid request"
    case serverError = "Server Error"
    case uknownError = "Uknown Error"
}

extension NetworkError {
    
    var localizedError: String {
        switch self {
        case .badRequest:
            return LS.Error.Network.badRequest
        case .encodingFailed:
            return LS.Error.Network.encodingFailed
        case .missingUrl:
            return LS.Error.Network.missingUrl
        case .serverError:
            return LS.Error.Network.serverError.localized()
        case .uknownError:
            return LS.Error.Network.unknownError.localized()
        }
    }
}
