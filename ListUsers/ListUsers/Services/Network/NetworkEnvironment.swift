//
//  NetworkEnvironment.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

enum NetworkEnvironment {
    case stage
    
    var baseUrl: String {
        return EnvironmentProviderImpl().loadEnviroment().baseUrl
    }
}
