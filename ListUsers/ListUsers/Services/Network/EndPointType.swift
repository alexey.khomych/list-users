//
//  EndPointType.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

protocol EndPointType {
    var environmentBaseURL: String { get }
    var baseUrl: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}

extension EndPointType {
    
    var environmentBaseURL : String {
        return NetworkAPI.environment.baseUrl
    }
    
    var baseUrl: URL {
        guard let url = URL(string: environmentBaseURL)
            else { fatalError("baseURL could not be configured.") }
        return url
    }
    
    var headers: HTTPHeaders? {
        return [
            "Content-Type": "application/json"
        ]
    }
}
