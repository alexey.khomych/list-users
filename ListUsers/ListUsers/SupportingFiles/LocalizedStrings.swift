//
//  LocalizedStrings.swift
//  ListUsers
//
//  Created by Alexey Khomych on 04.03.2023.
//

import Foundation

enum LS {
    enum Error {
        enum Network {
            static let title = "Error.Network.title".localized()
            static let okButtonTitle = "Error.Network.okButtonTitle".localized()
            static let unknownError = "Error.Network.unknownError".localized()
            static let notFoundError = "Error.Network.notFoundError".localized()
            static let noInternetConnection = "Error.Network.noInternetConnection".localized()
            static let serverError = "Error.Network.serverError".localized()
            static let encodingFailed = "Error.Network.encodingFailed".localized()
            static let missingUrl = "Error.Network.missingUrl".localized()
            static let badRequest = "Error.Network.badRequest".localized()
        }
    }
    
    enum Home {
        static let titleText = "Home.titleText".localized()
    }
}
